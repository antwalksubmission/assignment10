package org.antwalk;

import java.lang.reflect.Method;

import org.springframework.aop.AfterReturningAdvice;

public class AfterStudent implements AfterReturningAdvice {

	public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
		System.out.println("After Student.class methods executed");
	}

}
