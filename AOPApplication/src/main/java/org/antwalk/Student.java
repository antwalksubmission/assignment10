package org.antwalk;

import javax.sound.midi.Soundbank;

public class Student {
	private String name;
	private float engMarks;
	private float hindiMarks;
	private float mathsMarks;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getEngMarks() {
		return engMarks;
	}
	public void setEngMarks(float engMarks) {
		this.engMarks = engMarks;
	}
	public float getHindiMarks() {
		return hindiMarks;
	}
	public void setHindiMarks(float hindiMarks) {
		this.hindiMarks = hindiMarks;
	}
	public float getMathsMarks() {
		return mathsMarks;
	}
	public void setMathsMarks(float mathsMarks) {
		this.mathsMarks = mathsMarks;
	}
	
	public void avgMarks() {
		System.out.println("The average marks of "+this.name+" is "+(this.engMarks+this.hindiMarks+this.mathsMarks)/3);
	}
	
	public void showResult() {
		System.out.println();
		System.out.println("Hindi - "+this.hindiMarks+"\nEnglish - "+this.engMarks+"\nMaths - "+this.mathsMarks);
	}

}
